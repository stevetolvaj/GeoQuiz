package com.steve.geoquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.Gravity.TOP;

public class QuizActivity extends AppCompatActivity {

    private TextView mQuestionTextView;
    private double mScore = 0;
    private static final String TAG = "QuizActivity";
    private static  final String KEY_INDEX = "index";
    private static final String KEY_SCORE = "score";
    private static final String KEY_CHEATED = "cheated";
    // Request code will return integer for user cheating.
    private static final int REQUEST_CODE_CHEAT = 0;
    private static final int REQUEST_FINISH_RESTART = 1;
    private int mCurrentIndex = 0;
    private boolean mIsCheater;
    private boolean mRestart;
    private double mFinalScore;
    private CheckBox mCheckBox;

    private final Question[] mQuestionBank = new Question[] {
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Log onCreate.
        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mScore = savedInstanceState.getDouble(KEY_SCORE, 0);
            mIsCheater = savedInstanceState.getBoolean(KEY_CHEATED, false);
        }

        mCheckBox = (CheckBox) findViewById(R.id.checkBox);

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);

        Button trueButton = (Button) findViewById(R.id.true_button);
        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mQuestionBank[mCurrentIndex].getAnswered()) {
                    checkAnswer(true);
                } else {
                    alreadyTriedToAnswer();
                }
                mQuestionBank[mCurrentIndex].setAnswered(true);
                mCheckBox.setChecked(true);
            }
        });
        Button falseButton = (Button) findViewById(R.id.false_button);
        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mQuestionBank[mCurrentIndex].getAnswered()) {
                    checkAnswer(false);
                } else {
                    alreadyTriedToAnswer();
                }
                mQuestionBank[mCurrentIndex].setAnswered(true);
                mCheckBox.setChecked(true);
            }
        });

        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuestion();
            }
        });

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuestion();
            }
        });

        Button prevButton = (Button) findViewById(R.id.prev_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevQuestion();
            }
        });

        Button cheatButton = (Button) findViewById(R.id.cheat_button);
        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start cheat activity.
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this, answerIsTrue);
                // Use the startActivity if no requestCode is needed for cheating.
                // startActivity(intent);
                // Use below to know if user was cheating with requestCode being returned.
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

    } // End of onCreate.

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called");
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }

            mIsCheater = CheatActivity.wasAnswerShown(data);
        }
        if (requestCode == REQUEST_FINISH_RESTART) {
            if (data == null) {
                return;
            }
            mRestart = FinishedActivity.wasResetPressed(data);
            if (mRestart) {
                reset();
            }
        }
    }

    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);

        if (mQuestionBank[mCurrentIndex].getAnswered()) {
            mCheckBox.setChecked(true);
        } else {
            mCheckBox.setChecked(false);
        }


        // update score.
//        if (mCurrentIndex == 0) {
//            mScore = 0;
//        }

    }

    private void openFinishActivity() {
        Intent intent = FinishedActivity.newIntent(QuizActivity.this, mFinalScore);
        startActivityForResult(intent, REQUEST_FINISH_RESTART);
    }

    private void nextQuestion() {


        if (mCurrentIndex + 1 == mQuestionBank.length) {
            openFinishActivity();
        }
        if (!mQuestionBank[mCurrentIndex].getAnswered() && !mIsCheater) {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_layout));
            TextView text = (TextView) layout.findViewById(R.id.toast_text_view);
            text.setText(R.string.answer_before_proceed);
            Toast toast = new Toast(QuizActivity.this);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);


            toast.show();

            return;
        }
        mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;


            int question = mQuestionBank[mCurrentIndex].getTextResId();
            mQuestionTextView.setText(question);

//            if (mCurrentIndex == 0) {
//                mScore = 0;
//            }
            mIsCheater = false;
            updateQuestion();

    }

    private void prevQuestion() {
        mCurrentIndex = mCurrentIndex - 1;
        if (mCurrentIndex < 0) {
            mCurrentIndex = 0;
            return;
        }
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
        mIsCheater = false;
        updateQuestion();
    }

    private void alreadyTriedToAnswer() {
        Toast toast = Toast.makeText(QuizActivity.this,
                getString(R.string.already_answered),
                Toast.LENGTH_SHORT);
        toast.setGravity(TOP,0,0);
        toast.show();
    }

    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();

        int messageResId;

        if (mIsCheater) {
            messageResId = R.string.judgement_toast;
            mQuestionBank[mCurrentIndex].setAnswered(true);
        } else {

            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
                mScore++;
            } else {
                messageResId = R.string.incorrect_toast;
            }
        }



        Toast toast = Toast.makeText(QuizActivity.this,
                messageResId,
                Toast.LENGTH_SHORT);
        toast.setGravity(TOP,0,0);
        toast.show();

    }

    private void reset() {
        for (Question question : mQuestionBank) {
            question.setAnswered(false);
        }
        mCheckBox.setChecked(false);
        mRestart = false;
        mScore = 0;
    }

    // Logging methods below.

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    // This will save the question mCurrentIndex when orientation changed.

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);

        savedInstanceState.putDouble(KEY_SCORE, mScore);
        savedInstanceState.putBoolean(KEY_CHEATED, mIsCheater);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }
}