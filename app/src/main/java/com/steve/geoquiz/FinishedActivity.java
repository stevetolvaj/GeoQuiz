package com.steve.geoquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinishedActivity extends AppCompatActivity {
    private static final String EXTRA_RESTART_PRESSED = "com.steve.geoquiz.restart_pressed";
    private static final String EXTRA_SCORE = "com.steve.geoquiz.score";
    private Button mExitButton;
    private Button mRestartButton;
    private boolean mRestart;
    private TextView mScoreView;
    private double mScore;

    public static Intent newIntent(QuizActivity quizActivity, double score) {
        Intent intent = new Intent(quizActivity, FinishedActivity.class);
        intent.putExtra(EXTRA_SCORE, score);
        return intent;
    }

    public static boolean wasResetPressed(Intent data) {
        return data.getBooleanExtra(EXTRA_RESTART_PRESSED, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_finished);

        mScore = getIntent().getDoubleExtra(EXTRA_SCORE, 0.0);
        mScoreView = (TextView) findViewById(R.id.score_text);
        String result = new Double(Math.round(mScore)).toString();
        mScoreView.setText(result);


        mExitButton = (Button) findViewById(R.id.back_button);
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRestartButton = (Button) findViewById(R.id.restart_button);
        mRestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRestart = true;
                Intent data = new Intent();
                data.putExtra(EXTRA_RESTART_PRESSED, mRestart);
                setResult(RESULT_OK, data);
                onBackPressed();
            }
        });
    }
}