package com.steve.geoquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// Create new activity in the wizard, not using wizard, then must be declared in manifest to avoid
// exception.
public class CheatActivity extends AppCompatActivity {

    // Name for the packageContent for consistency.
    private static final String EXTRA_ANSWER_IS_TRUE = "com.steve.geoquiz.answer_is_true";
    private static final String EXTRA_ANSWER_SHOWN = "com.steve.geoquiz.answer_shown";


    private static final String KEY_CHEATED = "cheated";
    private static final String TAG = "CheatActivity";

    private boolean mCheated;
    private boolean mAnswerIsTrue;
    private TextView mAnswerTextView;
    private Button mShowAnswerButton;

    public static Intent newIntent(Context packageContent, boolean answerIsTrue) {
        Intent intent = new Intent(packageContent, CheatActivity.class);
        intent.putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue);
        return intent;
    }

    public static boolean wasAnswerShown(Intent result) {
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        if (savedInstanceState != null) {
            mCheated = savedInstanceState.getBoolean(KEY_CHEATED, false);
            Log.d(TAG, String.valueOf(mCheated));
        }

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);

        mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);

        mShowAnswerButton = (Button) findViewById(R.id.show_answer_button);
        mShowAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAnswerIsTrue) {
                    mAnswerTextView.setText(R.string.true_button);
                } else {
                    mAnswerTextView.setText(R.string.false_button);
                }
                   mCheated = true;
            }
        });
        Log.d(TAG, "onCreate: ");
    }

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }

    // Do not use call to super if passing back data.
    @Override
    public void onBackPressed() {
        setAnswerShownResult(mCheated);
        mCheated = false;
        finish();
    }

        @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_CHEATED, mCheated);
        Log.d(TAG, String.valueOf(mCheated));
   }
}